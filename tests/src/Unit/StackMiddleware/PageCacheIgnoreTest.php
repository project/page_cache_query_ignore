<?php

namespace Drupal\Tests\page_cache_query_ignore\Unit\StackMiddleware;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\page_cache_query_ignore\StackMiddleware\PageCacheIgnore;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @coversDefaultClass \Drupal\page_cache_query_ignore\StackMiddleware\PageCacheIgnore
 * @group page_cache_query_ignore
 */
class PageCacheIgnoreTest extends UnitTestCase {

  /**
   * Page cache ignore object configured on setUp().
   *
   * @var \Drupal\page_cache_query_ignore\StackMiddleware\PageCacheIgnore
   */
  protected $pageCacheIgnore;

  /**
   * Page cache ignore object configured on setUp().
   *
   * @var \Drupal\page_cache_query_ignore\StackMiddleware\PageCacheIgnore
   */
  protected $pageCacheIgnoreInclude;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $http_kernel = $this->createMock(HttpKernelInterface::class);
    $cache = $this->createMock(CacheBackendInterface::class);
    $request_policy = $this->createMock(RequestPolicyInterface::class);
    $response_policy = $this->createMock(ResponsePolicyInterface::class);
    $config_factory = $this->getConfigFactoryStub([
      'page_cache_query_ignore.settings' => [
        'query_parameters' => ['gclid', 'msclid', 's'],
        'ignore_action' => 'exclude',
      ],
    ]);
    $this->pageCacheIgnore = new PageCacheIgnore($http_kernel, $cache, $request_policy, $response_policy, $config_factory);

    $config_factory = $this->getConfigFactoryStub([
      'page_cache_query_ignore.settings' => [
        'query_parameters' => ['page', 'uid'],
        'ignore_action' => 'include',
      ],
    ]);
    $this->pageCacheIgnoreInclude = new PageCacheIgnore($http_kernel, $cache, $request_policy, $response_policy, $config_factory);
  }

  /**
   * Test the ignored parameters are loading correctly.
   *
   * @covers ::getDefinedQueryParameters
   */
  public function testGetQueryParameters() {
    $reflection = new \ReflectionClass(PageCacheIgnore::class);
    $method = $reflection->getMethod('getDefinedQueryParameters');
    $method->setAccessible(TRUE);

    $this->assertSame(['gclid', 'msclid', 's'], $method->invoke($this->pageCacheIgnore), "PageCacheIgnore::getDefinedQueryParameters matched settings");
  }

  /**
   * Test the excluded URL parameters are working as expected.
   *
   * @covers ::clear
   *
   * @dataProvider excludeProvider
   */
  public function testExcludeQueryParameters($input, $expected) {
    $reflection = new \ReflectionClass(PageCacheIgnore::class);
    $method = $reflection->getMethod('clear');
    $method->setAccessible(TRUE);

    $this->assertEquals($expected, $method->invokeArgs($this->pageCacheIgnore, [$input]), 'PageCacheIgnore::clear excluded correct parameters');
  }

  /**
   * Provides test data for testClear().
   */
  public function excludeProvider() {
    return [
      ['url/', 'url/'],
      ['url/?gclid=my-value', 'url/'],
      ['url/?a=abc&gclid=my-value', 'url/?a=abc'],
      ['url/?a=abc&gclid=my-value&b=def', 'url/?a=abc&b=def'],
      ['url/?a=abc&gclid=my-value&msclid=my-value2&b=def', 'url/?a=abc&b=def'],
      ['url/gclid', 'url/gclid'],
      ['url/?a=gclid&msclid=def', 'url/?a=gclid'],
      ['url/keys?keys=abc&s=123', 'url/keys?keys=abc'],
      ['url/?s[]=abc&s[]=def&s[]=ghi', 'url/'],
      ['?s=abc', ''],
      ['?a=def', '?a=def'],
    ];
  }

  /**
   * Test the included URL parameters are working as expected.
   *
   * @covers ::clear
   *
   * @dataProvider includeProvider
   */
  public function testIncludeQueryParameters($input, $expected) {
    $reflection = new \ReflectionClass(PageCacheIgnore::class);
    $method = $reflection->getMethod('clear');
    $method->setAccessible(TRUE);

    $this->assertEquals($expected, $method->invokeArgs($this->pageCacheIgnoreInclude, [$input]), 'PageCacheIgnore::clear included correct parameters');
  }

  /**
   * Provides test data for testClear().
   */
  public function includeProvider() {
    return [
      ['url/', 'url/'],
      ['url/?gclid=my-value', 'url/'],
      ['url/?page=1&gclid=my-value', 'url/?page=1'],
      ['url/?page=1&gclid=my-value&uid=me', 'url/?page=1&uid=me'],
    ];
  }

}
