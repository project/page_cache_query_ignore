## Introduction

Page Cache Ignore query parameters improves the performance of the page_cache
module by removing client side variables from the cache key.

As an example: with the Drupal core page_cache implementation, the following
URLs will be considered different.

 * https://my.site/
 * https://my.site/?gclid=abc

By setting the ignored query parameters to `gclid`, these links will be
considered identical by the cache and performance will be greatly improved. This
will also reduce the size of your cache.

This module is not a replacement to a properly configured CDN. However, it will
provide a significant improvement where a CDN is not an option or desired.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

* Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


## Configuration

  1. Configure the query parameters that you wish to ignore in `Administration >
     Configuration > Development > Performance > Page cache query ignore`.
  2. Provide a listing of query parameters to ignore in the 'Ignored query
     parameters' field, then click Submit.


## Maintainers

* Vyacheslav Malchik - https://www.drupal.org/u/validoll
* Nathan ter Bogt - https://www.drupal.org/u/nterbogt
