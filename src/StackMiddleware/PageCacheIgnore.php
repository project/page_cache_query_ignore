<?php

namespace Drupal\page_cache_query_ignore\StackMiddleware;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Drupal\page_cache\StackMiddleware\PageCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Executes the page caching before the main kernel takes over the request.
 *
 * Ignore query parameters also.
 */
class PageCacheIgnore extends PageCache {

  /**
   * A config object for the page cache query parameters ignore.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * The defined query parameters.
   *
   * @var array
   */
  protected $definedQueryParameters = [];

  /**
   * The ignore action.
   *
   * @var string
   */
  protected $ignoreAction = '';

  /**
   * {@inheritdoc}
   */
  public function __construct(HttpKernelInterface $http_kernel, CacheBackendInterface $cache, RequestPolicyInterface $request_policy, ResponsePolicyInterface $response_policy, ConfigFactoryInterface $config_factory) {
    parent::__construct($http_kernel, $cache, $request_policy, $response_policy);
    $this->configFactory = $config_factory;
  }

  /**
   * Get defined query parameters.
   *
   * @return array
   *   Ignored params.
   */
  protected function getDefinedQueryParameters() {
    if (empty($this->definedQueryParameters)) {
      $this->definedQueryParameters = $this->configFactory
        ->get('page_cache_query_ignore.settings')
        ->get('query_parameters');
    }
    return $this->definedQueryParameters;
  }

  /**
   * Get the ignore action.
   *
   * @return string
   *   The ignore action.
   */
  protected function getIgnoreAction() {
    if (empty($this->ignoreAction)) {
      $this->ignoreAction = $this->configFactory
        ->get('page_cache_query_ignore.settings')
        ->get('ignore_action');
    }
    return $this->ignoreAction;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCacheId(Request $request) {
    // Once a cache ID is determined for the request, reuse it for the duration
    // of the request. This ensures that when the cache is written, it is only
    // keyed on request data that was available when it was read. For example,
    // the request format might be NULL during cache lookup and then set during
    // routing, in which case we want to key on NULL during writing, since that
    // will be the value during lookups for subsequent requests.
    if (!isset($this->cid)) {
      $cid_parts = [
        $request->getSchemeAndHttpHost() . $this->clear($request->getRequestUri()),
        $request->getRequestFormat(NULL),
      ];
      $this->cid = implode(':', $cid_parts);
    }
    return $this->cid;
  }

  /**
   * Clear query string.
   *
   * @param string $value
   *   The value to cleanup.
   *
   * @return string
   *   The cleared value.
   */
  protected function clear($value) {
    $request_parts = UrlHelper::parse($value);

    if (empty($request_parts['query'])) {
      return $value;
    }

    $request_uri = '';

    if (!empty($request_parts['path'])) {
      $request_uri .= $request_parts['path'];
    }

    if ($this->getIgnoreAction() === 'include') {
      // Keep the query parameters that are included.
      $request_query = $this->keepIncludedQueryParameters($request_parts['query']);
    }
    else {
      // Remove the query arguments that are excluded.
      $request_query = $this->removeExcludedQueryParameters($request_parts['query']);
    }

    if (!empty($request_query)) {
      $request_uri .= '?' . UrlHelper::buildQuery($request_query);
    }

    return $request_uri;
  }

  /**
   * Remove the excluded query parameters.
   *
   * @param array $query_parts
   *   The query parts.
   *
   * @return array
   *   The modified query parts.
   */
  protected function removeExcludedQueryParameters(array $query_parts) {
    return UrlHelper::filterQueryParameters($query_parts, $this->getDefinedQueryParameters());
  }

  /**
   * Keep the included query parameters.
   *
   * @param array $query_parts
   *   The query parts.
   *
   * @return array
   *   The modified query parts.
   */
  protected function keepIncludedQueryParameters(array $query_parts) {
    $keep = array_flip($this->getDefinedQueryParameters());
    return array_intersect_key($query_parts, $keep);
  }

  /**
   * Public wrapper around protected PageCache::storeResponse().
   *
   * BigPipe Sessionless needs public access to storeResponse(), so
   * to keep compatibility, we include this as well.
   */
  // @codingStandardsIgnoreStart
  public function _storeResponse(Request $request, Response $response) {
    return $this->storeResponse($request, $response);
  }
  // @codingStandardsIgnoreEnd

}
